# MarkDown Doc Service

## 环境要求：

#### nodejs：版本4.0+


#### git

git 用于更新拉取md文档内容。

#### nginx

暂时可以先不配，可以配置。

## 服务安装

#### 1、从 git 拉取代码。

```shell
git clone https://gitee.com/geliang/MarkDownNodeWebServer.git
```

#### 2、配置端口号

默认3000,有需要可以打开 example 目录下 multiple-instances.js 文件定位到最后，配置端口号。

```shell
vim ./example/multiple-instances.js
```

定位到如下代码，修改你想要配置的端口号。

```javascript
const server = mainApp.listen(3000, function () {
  debug('Express HTTP server listening on port ' + server.address().port);
});
```

修改 multiple-instances.js 文件为可执行权限

```shell
chmod +x multiple-instances.js
```

#### 配置文档 webhook 脚本

打开文件 ./example/scripts/pulldoc.sh,配置你的文档源git地址

```shell
vim ./example/scripts/pulldoc.sh

### 文件内容大致如下
#!/bin/bash
pwd
cd example
if [ ! -d "content" ];then
    git clone git:xxxxxxxxxxxxxx content
fi
cd content

git pull

```

把 pulldoc.sh 文件内容中 git 地址改为文档的 git 地址。

> **注意** git 地址后面一定要加 content 字段, content为文档源存放目录.



#### 启动服务

在 root 目录下安装依赖

```shell
npm install
```

调试开发时可以直接运行server.js启动

在root目录下使用 pm2 启动服务，服务托管于 pm2 工具，所以需要安装 pm2 工具。

```shell
## 安装 pm2
npm install pm2 -g
## 运行服务
pm2 start --name=MarkDownDocServer npm -- start
```

查看服务日志是否启动成功

```shell
pm2 logs MarkDownDocServer
# 或者使用 pm2 list 查看 列表服务。
```

打开浏览器 输入地址+端口号 看看是否开启成功。

```
http://host:port
```

#### 服务其他操作

使用 pm2 查看，删除、重启服务。

查看服务名称和ID

```
pm2 list 
```

删除服务

```
pm2 delete [id或者服务名称]
```

重启服务

```
pm2 retart [id或者服务名称]
```





## Nginx 配置

暂时 nginx 代理配置可要可以不要。但建议上https+域名方便google收录

