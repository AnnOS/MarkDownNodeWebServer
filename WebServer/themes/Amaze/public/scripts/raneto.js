;(function ($) {

  'use strict';
  //填充table样式
  $(document).ready(function () {

    // Add amazeui  styling to tables
    $('table').addClass('am-table am-table-bordered  am-table-hover am-table-striped  .am-table-striped am-table-compact');
  });

  //自动展开导航栏
  $(document).ready(function (e) {
    $("li").click(function (e) {
      $(this).children("ul").toggle();
      e.stopPropagation();
    });
    $("ul li ul").toggle();
    $(".category.active").children("ul").toggle();
    $("#clicktonavi").click(function (e) {
      $("#directory_navigation_container").toggle();
      e.stopPropagation();
    });


    console.log("[INFO]  ss "+( $(".am-active").offset().top));
    //滚动导航栏
    $('#navi_right').animate({
      scrollTop: $(".am-active").offset().top
    }, 1000);
  });


})(jQuery);
