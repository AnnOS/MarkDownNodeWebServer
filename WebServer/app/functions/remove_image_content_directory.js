'use strict';
// content_dir
var summary;

function remove_image_content_directory(config, pageList) {
  var temp = [];
  for (var i = 0; i < pageList.length; i++) {
    if (pageList[i].slug === config.image_url.replace(/\//g, '')
      || (pageList[i].slug.toLowerCase().endsWith(".assets") && pageList[i].is_directory)) {
      console.log("[INFO] pageList remove image dir:" + pageList[i].slug);
    }else{
      temp.push(pageList[i]);
    }
  }
  pageList = temp;
  try {
    if (!summary) {
      var fs = require("fs");
      var dir = config.content_dir + "/summary.json";
      if (fs.existsSync(dir)) {
        summary = JSON.parse(fs.readFileSync(config.content_dir + "/summary.json").toString('utf-8'));
        // console.log("[INFO] sum " + JSON.stringify(summary));
      }
    }
    if (summary) {
      for (var j = 0; j < summary.length; j++) {
        var s = summary[j];
        for (var k = 0; k < pageList.length; k++) {
          var page = pageList[k];
          if(s.file ===( "/"+page.slug)){
            page.title = s.alias;
          }
        }

      }
    }
  } catch (e) {
    console.log("[INFO] e " + e);
  }

  return pageList;
}

// Exports
module.exports = remove_image_content_directory;
