# 支持MarkDown(MD)的Blog博客Web系统 

此博客基于Raneto二次开发, base on Node.js 

源代码地址:https://gitee.com/geliang/MarkDownNodeWebServer

> 示例网站 :[http://fluttergo.com](http://fluttergo.com/) 

---

## 特性

相比Raneto,改动如下

特性:

- 增加一套主题

- 支持git仓库作为bolg的内容源,并提供git webhook做到推送即发布

- 支持目录导航,排序,中英文路径,路径别名

- 支持相对地址的图文链接(无需额外图床)

- 支持文章排序,文章内段落跳转,标题别名

  

todo:

- 支持中英文关键字搜索

## 安装说明
熟悉node的 几分钟就可以跑起来,详细见 [./WebServer/README.md](./WebServer/README.md)



> Ranto官网: [http://docs.raneto.com](http://docs.raneto.com) 
>
> to see a demo and more...

